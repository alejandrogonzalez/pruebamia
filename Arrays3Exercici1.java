import java.text.DecimalFormat;

public class Arrays3Exercici1 {
    public static final double maxbarra = 70, array = 13.00;

    public static void main(String[] args) {

        double[] vector = new double[(int) array];
        int i = 0, j = 0, x = 0;
        int LongitudBarra = 0;
        double valorMaxim;

        valorMaxim = vector[0];

        for (i = 0; i < vector.length; i++) {
            vector[i] = (Math.random() * 1000);
        }


        for (i = 0; i < vector.length; i++) {

            if (valorMaxim < vector[i]) {
                valorMaxim = vector[i];
            }
        }
        DecimalFormat formato1 = new DecimalFormat("#.00");
        System.out.println("El gráfico quedaría de la siguiente forma: \n"
                + "j");

        for(j=0;j<vector.length;j++) {

            LongitudBarra=(int)(maxbarra*(vector[j]/valorMaxim));
            if(vector[j]<9.99) {
                System.out.print(" "+(formato1.format(vector[j])) + "   | " );
            }else if(vector[j]>99.99) {
                System.out.print(" "+(formato1.format(vector[j])) + " | " );
            }else{
                System.out.print(" "+(formato1.format(vector[j])) + "  | " );
            }

            for(x=0;x<LongitudBarra;x++) {

                System.out.print("=");
            }
            System.out.println(" ");
        }





    }
}
